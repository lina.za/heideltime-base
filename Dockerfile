# This file is a template, and might need editing before it works on your project.
FROM python:3.6

WORKDIR /app
#install heideltime with special rules
RUN cd $WORKDIR
RUN wget https://www.dropbox.com/s/y33iftyd6wdvb1f/thesis_extras.zip
RUN unzip thesis_extras.zip

#install tree-tagger
RUN mkdir tagger
WORKDIR /app/tagger
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.2.tar.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz
RUN chmod +x ./install-tagger.sh
RUN ./install-tagger.sh

#COPY requirements.txt /usr/src/app/
#RUN pip install --no-cache-dir -r requirements.txt

#COPY . /usr/src/app

# For Django
#EXPOSE 8000
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
CMD ["echo", "test"]
